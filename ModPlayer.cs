using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;
using Terraria;
using Terraria.ID;
using Terraria.IO;
using Terraria.ModLoader;

namespace PlayerTeamIcon {
	public class MPlayer : ModPlayer {
		float alpha = 1f;
		float borderAlpha = 1f;
		public override void DrawEffects(PlayerDrawInfo drawInfo, ref float r, ref float g, ref float b, ref float a, ref bool fullBright) {
			if (Main.netMode == 1 && !Main.gameMenu && !Main.ingameOptionsWindow) {
				for(int index = 0; index < Main.player.Length; ++index) {
					Player player = Main.player[index];
					if (player.active) {
						DrawTeamIcon(player);
					}
				}
			}
		}

		private void DrawTeamIcon(Player player) {
			if (player.dead) { return; }
			if (player == Main.player[Main.myPlayer]) {
				if (Main.teamCooldown > 0) {
					alpha = 1f;
					borderAlpha = (float)Main.teamCooldown / (float)Main.teamCooldownLen;
				}
				else {
					borderAlpha = 0f;
					if (alpha > Config.Client.YourAlpha) {
						alpha -= 0.01f;
					}
				}
				Vector2 drawPos = new Vector2((int)player.Center.X, (int)player.Center.Y + 28);
				if (player.hostile) {
					if (Main.teamCooldown > 0) {
						Main.spriteBatch.Draw(Main.PVPTexture[0], new Vector2(drawPos.X - 18f - Main.screenPosition.X, drawPos.Y - 98f - Main.screenPosition.Y), new Rectangle?(new Rectangle(114, (int)(player.team * 38), 36, 36)), new Color(1f, 1f, 1f, 1f) * borderAlpha, 0f, new Vector2(0f, 0f), 1f, SpriteEffects.None, 0f);
					}
					Main.spriteBatch.Draw(Main.PVPTexture[0], new Vector2(drawPos.X - 18f - Main.screenPosition.X, drawPos.Y - 98f - Main.screenPosition.Y), new Rectangle?(new Rectangle(76, (int)(player.team * 38), 36, 36)), new Color(1f, 1f, 1f, 1f) * alpha, 0f, new Vector2(0f, 0f), 1f, SpriteEffects.None, 0f);
				}
				else {
					if (Main.teamCooldown > 0) {
						Main.spriteBatch.Draw(Main.PVPTexture[2], new Vector2(drawPos.X - 10f - Main.screenPosition.X, drawPos.Y - 90f - Main.screenPosition.Y), new Rectangle?(new Rectangle(0, 0, 20, 20)), new Color(1f, 1f, 1f, 1f) * borderAlpha, 0f, new Vector2(0f, 0f), 1f, SpriteEffects.None, 0f);
					}
					Main.spriteBatch.Draw(Main.PVPTexture[1], new Vector2(drawPos.X - 8f - Main.screenPosition.X, drawPos.Y - 88f - Main.screenPosition.Y), new Rectangle?(new Rectangle((int)(player.team * 18), 0, 16, 16)), new Color(1f, 1f, 1f, 1f) * alpha, 0f, new Vector2(0f, 0f), 1f, SpriteEffects.None, 0f);
				}
			}
			else {
				Vector2 drawPos = new Vector2((int)player.Center.X, (int)player.Center.Y + 28);
				if (player.hostile) {
					Main.spriteBatch.Draw(Main.PVPTexture[0], new Vector2(drawPos.X - 18f - Main.screenPosition.X, drawPos.Y - 98f - Main.screenPosition.Y), new Rectangle?(new Rectangle(76, (int)(player.team * 38), 36, 36)), new Color(1f, 1f, 1f, 1f) * Config.Client.OtherAlpha, 0f, new Vector2(0f, 0f), 1f, SpriteEffects.None, 0f);
				}
				else {
					Main.spriteBatch.Draw(Main.PVPTexture[1], new Vector2(drawPos.X - 8f - Main.screenPosition.X, drawPos.Y - 88f - Main.screenPosition.Y), new Rectangle?(new Rectangle((int)(player.team * 18), 0, 16, 16)), new Color(1f, 1f, 1f, 1f) * Config.Client.OtherAlpha, 0f, new Vector2(0f, 0f), 1f, SpriteEffects.None, 0f);
				}
			}
		}
	}
}