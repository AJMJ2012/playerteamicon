﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System;
using Terraria.ID;
using Terraria.IO;
using Terraria.ModLoader;
using Terraria;

namespace PlayerTeamIcon {
	public class PlayerTeamIcon : Mod {
		public PlayerTeamIcon() {
			Properties = new ModProperties() {
				Autoload = true
			};
		}

		bool LoadedFKTModSettings = false;
		public override void Load() {
			if (Main.netMode == 2) { return; }
			Config.ReadConfig();
			LoadedFKTModSettings = ModLoader.GetMod("FKTModSettings") != null;
			if (LoadedFKTModSettings) {
				try { LoadModSettings(); }
				catch (Exception e) {
					DALib.Logger.ErrorLog("Unable to Load Mod Settings", Config.modPrefix);
					DALib.Logger.ErrorLog(e, Config.modPrefix);
				}
			}
		}

		public override void PostUpdateInput() {
			if (LoadedFKTModSettings && !Main.gameMenu && Main.netMode != 2) {
				if (DALib.DALib.tick % 60 == 0) {
					try {
						List<Type> Types = new List<Type>{ typeof(Config.Global) };
						if (Main.netMode != 2) { Types.Add(typeof(Config.Client)); }
						if (Main.netMode != 1) { Types.Add(typeof(Config.Server)); }
						string OldConfig = null;
						foreach (Type type in Types) { OldConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						UpdateModSettings();
						string NewConfig = null;
						foreach (Type type in Types) { NewConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						if (OldConfig != NewConfig) { Config.SaveConfig(); }
					}
					catch (Exception e) {
						DALib.Logger.ErrorLog("Unable to compare config data", Config.modPrefix);
						DALib.Logger.ErrorLog(e, Config.modPrefix);
					}
				}
			}
		}

		private void LoadModSettings() {
			FKTModSettings.ModSetting setting = FKTModSettings.ModSettingsAPI.CreateModSettingConfig(this);
			setting.EnableAutoConfig();
			setting.AddFloat("YourAlpha", "Max alpha of your icon", 0f, 1f, false);
			setting.AddFloat("OtherAlpha", "Alpha of other player icons", 0f, 1f, false);
		}

		private void UpdateModSettings() {
			FKTModSettings.ModSetting setting;
			if (FKTModSettings.ModSettingsAPI.TryGetModSetting(this, out setting))  {
				setting.Get("YourAlpha", ref Config.Client.YourAlpha);
				setting.Get("OtherAlpha", ref Config.Client.OtherAlpha);
			}
		}
	}

	public static class Config {
		public static class Global {}
		public static class Client {
			public static float YourAlpha = 0.1f;
			public static float OtherAlpha = 1f;
		}
		public static class Server {}

		public static string modName = "PlayerTeamIcon";
		public static string modPrefix = "PTI";
		private static Preferences Configuration = new Preferences(Path.Combine(Main.SavePath, "Mod Configs/" + modName + ".json"));

		public static void ReadConfig() {
			if(Configuration.Load()) {
				Configuration.Get("YourAlpha", ref Client.YourAlpha);
				Configuration.Get("OtherAlpha", ref Client.OtherAlpha);
				DALib.Logger.DebugLog("Config Loaded", Config.modPrefix);
			}
			else {
				DALib.Logger.DebugLog("Creating Config", Config.modPrefix);
			}
			SaveConfig();
		}

		public static void ClampConfig() {
			Client.YourAlpha = MathHelper.Clamp(Client.YourAlpha, 0, 1);
			Client.OtherAlpha = MathHelper.Clamp(Client.OtherAlpha, 0, 1);
		}

		public static void SaveConfig() {
			ClampConfig();
			Configuration.Clear();
			Configuration.Put("YourAlpha", Client.YourAlpha);
			Configuration.Put("OtherAlpha", Client.OtherAlpha);
			Configuration.Save();
			DALib.Logger.DebugLog("Config Saved", Config.modPrefix);
		}
	}

	public class Commands : ModCommand {
		public override CommandType Type {
			get { return CommandType.Chat; }
		}
		public override string Command {
			get { return Config.modPrefix.ToLower(); }
		}
		public override string Description {
			get { return mod.Name; }
		}
		public override string Usage {
			get { return Command + " reload"; }
		}
		public override void Action(CommandCaller caller, string input, string[] args) {
			switch (args[0].ToLower()) {
				case "reload":
					DALib.Logger.Log("Config Reloaded");
					Config.ReadConfig();
					return;
			}
		}
	}
}
